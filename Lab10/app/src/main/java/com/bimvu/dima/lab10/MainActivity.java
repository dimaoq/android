package com.bimvu.dima.lab10;

import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView lblText;
    private TextView lblCaption;
    private boolean mLayoutDown;
    private LinearLayout layoutMenu;
    private int mLayoutHeight;
    private int mLayoutMenuHeight;
    private int mHeaderHeight;
    private int pos;
    private RelativeLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLayoutDown = true;

        lblCaption = (TextView) findViewById(R.id.lblCaption);
        lblText = (TextView) findViewById(R.id.lblText);

        final Button btnFirst = (Button) findViewById(R.id.btnFirst);
        final Button btnSecond = (Button) findViewById(R.id.btnSecond);

        if (btnFirst != null) {
            btnFirst.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lblText.setText("Текст из первой кнопки");
                }
            });
        }

        if (btnSecond != null) {
            btnSecond.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lblText.setText("Текст из второй кнопки");
                }
            });
        }

        layoutMenu = (LinearLayout) findViewById(R.id.layMenu);
        mainLayout = (RelativeLayout) findViewById(R.id.layMain);
        mainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mLayoutHeight = mainLayout.getHeight();
                mHeaderHeight = lblCaption.getHeight();
                mLayoutMenuHeight = layoutMenu.getHeight();

                pos = layoutMenu.getTop();
                //layoutMenu.setY(mLayoutHeight - mHeaderHeight);

                //pos = ((RelativeLayout.LayoutParams)(layoutMenu.getLayoutParams())).topMargin;
                //layoutMenu.layout(layoutMenu.getLeft(), (int)(mLayoutHeight * 0.9), layoutMenu.getRight(), (int)(mLayoutHeight * 0.9) + layoutMenu.getHeight());
                //layoutMenu.setTop((int)(mLayoutHeight * 0.9));
//                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) layoutMenu.getLayoutParams();
//                lp.topMargin = (int)(mLayoutHeight * 0.8);
//                layoutMenu.setLayoutParams(lp);
                mainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        if (lblCaption != null) {
            lblCaption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLayoutDown){
                        Animation anim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.uprising);
                        mainLayout.setDrawingCacheEnabled(false);
                        layoutMenu.setDrawingCacheEnabled(false);
                        anim.setAnimationListener(new AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
//                                layoutMenu.setTop(pos);
////                                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) layoutMenu.getLayoutParams();
////                                lp.topMargin += pos;
////                                layoutMenu.setLayoutParams(lp);
//                                //layoutMenu.setY(pos);

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                //RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) layoutMenu.getLayoutParams();
//                                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
//                                //lp.addRule(RelativeLayout.ALIGN_PARENT_TOP,  RelativeLayout.TRUE);
////                                lp.leftMargin = 0;
//                                //lp.topMargin = pos;
//                                Log.d("Fjdf", Integer.toString(pos));
//
//                                lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                                lp.topMargin = mLayoutHeight - mHeaderHeight - mLayoutMenuHeight;
//                                layoutMenu.setLayoutParams(lp);
                                layoutMenu.setY(mLayoutHeight - mHeaderHeight - mLayoutMenuHeight);


//                                //layoutMenu.setLeft(0);
//                                //layoutMenu.setY(pos);
//                                RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) layoutMenu.getLayoutParams();
                                //layoutMenu.invalidate();
//                                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) layoutMenu.getLayoutParams();
//                                lp.topMargin = (int)(mLayoutHeight * 0.1);
//                                //lp.bottomMargin = lp.topMargin + layoutMenu.getHeight();
//
//                                layoutMenu.setLayoutParams(lp);


                                //layoutMenu.layout(layoutMenu.getLeft(), (int)(mLayoutHeight * 0.1), layoutMenu.getRight(), layoutMenu.getHeight() + (int)(mLayoutHeight * 0.1));
                                //layoutMenu.setTop((int)(mLayoutHeight * 0.9));
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });
                        layoutMenu.startAnimation(anim);
                        mLayoutDown = false;
                    }
                    else{
                        Animation anim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.downside);
                        anim.setAnimationListener(new AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {

                                layoutMenu.setY(mLayoutHeight - mHeaderHeight);
//                                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) layoutMenu.getLayoutParams();
//                                lp.topMargin = (int)(mLayoutHeight * 0.8);
//                                layoutMenu.setLayoutParams(lp);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });
                        layoutMenu.startAnimation(anim);
                        mLayoutDown = true;
                    }
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (null != layoutMenu) {
            int layHeight = layoutMenu.getHeight();

            int headerHeight = lblCaption.getHeight();
        }
    }
}
