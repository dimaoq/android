package com.bimvu.dima.lab2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        Intent runIntent = getIntent();
        Bundle bundle = runIntent.getExtras();
        if (bundle != null && bundle.getBoolean("returned")){
            TextView lblReturned = (TextView) findViewById(R.id.lblReturned);
            lblReturned.setText(R.string.returned_text);
        }

        Button btnSendToSecondActivity = (Button) findViewById(R.id.btnSendToSecondActivity);
        final TextView txtMessage = (TextView) findViewById(R.id.txtMessage);

        btnSendToSecondActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
                intent.putExtra("information", txtMessage.getText().toString());
                startActivity(intent);
            }
        });
    }
}
