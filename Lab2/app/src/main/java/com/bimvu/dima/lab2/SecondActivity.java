package com.bimvu.dima.lab2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {

    private String gotInformation = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent runIntent = getIntent();
        Bundle bundle = runIntent.getExtras();
        if (bundle != null){
            gotInformation = bundle.getString("information");
        }

        Button btnSendToThirdActivity = (Button) findViewById(R.id.btnSendToThirdActivity);
        btnSendToThirdActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                intent.putExtra("information", gotInformation);
                startActivity(intent);
            }
        });
    }
}
