package com.bimvu.dima.lab2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Intent runIntent = getIntent();
        Bundle bundle = runIntent.getExtras();
        if (bundle != null) {
            String information = bundle.getString("information");
            TextView lblInfo = (TextView) findViewById(R.id.lblInfo);
            lblInfo.setText(information);
        }

        Button btnReturnToFirstActivity = (Button) findViewById(R.id.btnReturnToFirstActivity);
        btnReturnToFirstActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ThirdActivity.this, FirstActivity.class);
                intent.putExtra("returned", true);
                startActivity(intent);
            }
        });
    }
}
