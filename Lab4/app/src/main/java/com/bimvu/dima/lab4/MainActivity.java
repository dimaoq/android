package com.bimvu.dima.lab4;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import data.Student;
import dbmanager.DatabaseManager;
import utils.Utils;

public class MainActivity extends AppCompatActivity {
    private static final String SHARED_PREFS_NAME = "StudentsPreferences";
    private static final String SHARED_PREFS_KEY_NAME = "firstLaunch";
    private static final String LOG_TAG = "TAG";
    private static final String LOG_DB_INSERT_FAILED = "DB INSERT FAILED";

    private TextView lblStudentInfo;
    private TextView lblLoadedStudentInfo;
    private Button btnCreateDB;
    private Button btnLoadDB;
    private DatabaseManager dbManager;
    private SQLiteDatabase db;

    private static final Student[] STUDENTS = {
            new Student("Никита", "Иванов", "ПС", 31),
            new Student("Александр", "Пурышев", "ПС", 32),
            new Student("Сергей", "Смышляев", "УиТС", 31)
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbManager = new DatabaseManager(this);
        db = dbManager.getWritableDatabase();
        lblStudentInfo = (TextView) findViewById(R.id.lblStudentInfo);
        lblLoadedStudentInfo = (TextView) findViewById(R.id.lblLoadedStudentInfo);
        btnCreateDB = (Button) findViewById(R.id.btnCreateDB);
        btnLoadDB = (Button) findViewById(R.id.btnLoadDB);

        SharedPreferences sp = getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE);
        if (sp.getBoolean(SHARED_PREFS_KEY_NAME, true))
        {
            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean(SHARED_PREFS_KEY_NAME, false);
            editor.commit();
        }
        else{
            btnCreateDB.setVisibility(View.GONE);
        }

        btnCreateDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null == db || !db.isOpen()){
                    db = dbManager.getWritableDatabase();
                }

                String outString = "";
                for (Student student : STUDENTS){
                    ContentValues cv = new ContentValues();
                    cv.put(DatabaseManager.COLUMN_NAME_FIRST_NAME, student.getFirstName());
                    cv.put(DatabaseManager.COLUMN_NAME_LAST_NAME, student.getLastName());
                    cv.put(DatabaseManager.COLUMN_NAME_SPECIALITY, student.getSpeciality());
                    cv.put(DatabaseManager.COLUMN_NAME_GROUP, student.getGroup());

                    long result = db.insert(DatabaseManager.TABLE_NAME, null, cv);
                    if (result == -1){
                        Log.d(LOG_TAG, LOG_DB_INSERT_FAILED);
                    }

                    outString += student.toString();
                }

                lblStudentInfo.setText(outString);
                db.close();
            }
        });

        btnLoadDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null == db || !db.isOpen()){
                    db = dbManager.getReadableDatabase();
                }

                Cursor cursor = db.query(DatabaseManager.TABLE_NAME, DatabaseManager.COLUMN_PROJECTTION, null, null, null, null, null);


                if (null != cursor && cursor.getCount() > 0) {
                    if (cursor.moveToFirst() && !cursor.isAfterLast()) {
                        ArrayList<Student> students = new ArrayList<>();
                        do {
                            String firstName = cursor.getString(cursor.getColumnIndex(DatabaseManager.COLUMN_NAME_FIRST_NAME));
                            String lastName = cursor.getString(cursor.getColumnIndex(DatabaseManager.COLUMN_NAME_LAST_NAME));
                            String speciality = cursor.getString(cursor.getColumnIndex(DatabaseManager.COLUMN_NAME_SPECIALITY));
                            int group = cursor.getInt(cursor.getColumnIndex(DatabaseManager.COLUMN_NAME_GROUP));

                            Student student = new Student(firstName, lastName, speciality, group);
                            students.add(student);
                        } while (cursor.moveToNext());
                        Utils.writeToFile(MainActivity.this, Utils.FILE_NAME, students);
                    }
                }
                db.close();

                String outString = "";
                ArrayList<Student> students = (ArrayList<Student>) Utils.readFromFile(MainActivity.this, Utils.FILE_NAME);
                if (students.size() > 0){
                    for (Student student : students){
                        outString += student.toString() + "-----\n";
                    }
                }
                else{
                    outString = "Empty data";
                }
                lblLoadedStudentInfo.setText(outString);
            }
        });
    }
}