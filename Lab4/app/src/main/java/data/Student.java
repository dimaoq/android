package data;

/*
Разработать приложение:
- 1й старт приложения - на экране 2 кнопки - 1я - создает список из объектов и загружает его в БД,
  2я кнопка ЧИТАЕТ ИЗ БД и записывает В ФАЙЛ НА ДИСК (не важно - внутренний или внешний)
- 2й и последующий старты - на экране только 2я кнопка.
 */

import java.io.Serializable;

public class Student implements Serializable{

    private String firstName;
    private String lastName;
    private String speciality;
    private int group;

    public Student(String firstName, String lastName, String speciality, int group){
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.speciality = speciality;
        this.group = group;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    @Override
    public String toString() {
        String res = "";
        res += "First name : " + this.firstName + "\n";
        res += "Last name : " + this.lastName + "\n";
        res += "Speciality : " + this.speciality + "\n";
        res += "Group : " + this.group + "\n";

        return res;
    }
}
