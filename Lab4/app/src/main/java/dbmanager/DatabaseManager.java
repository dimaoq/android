package dbmanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseManager extends SQLiteOpenHelper{
    private static final String DB_NAME = "students.db";
    private static final int DB_VERSION = 1;
    public static final String TABLE_NAME = "Students";
    public static final String COLUMN_NAME_ID = "id";
    public static final String COLUMN_NAME_FIRST_NAME = "firstName";
    public static final String COLUMN_NAME_LAST_NAME = "lastName";
    public static final String COLUMN_NAME_SPECIALITY = "speciality";
    public static final String COLUMN_NAME_GROUP = "studentGroup";

    public static final String[] COLUMN_PROJECTTION = {
            COLUMN_NAME_ID, COLUMN_NAME_FIRST_NAME, COLUMN_NAME_LAST_NAME, COLUMN_NAME_SPECIALITY,
            COLUMN_NAME_GROUP
    };

    public DatabaseManager(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" + COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME_FIRST_NAME + " TEXT, " + COLUMN_NAME_LAST_NAME + " TEXT, " + COLUMN_NAME_SPECIALITY + " TEXT, " +
                    COLUMN_NAME_GROUP + " INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
