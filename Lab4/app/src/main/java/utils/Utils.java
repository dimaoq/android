package utils;

import android.app.Activity;
import android.content.Context;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Utils {
    public static final String FILE_NAME = "students.txt";

    public static void writeToFile(Context context, String fileName, Object object){
        ObjectOutputStream objOutput = null;
        FileOutputStream fileOutput = null;
        try{
            fileOutput = context.openFileOutput(fileName, Activity.MODE_PRIVATE);
            objOutput = new ObjectOutputStream(fileOutput);
            objOutput.writeObject(object);
            fileOutput.getFD().sync();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        finally {
            if (null != fileOutput){
                try{
                    fileOutput.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }

            if (null != objOutput){
                try{
                    objOutput.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

    public static Object readFromFile(Context context, String fileName){
        ObjectInputStream objIn = null;
        FileInputStream fileIn = null;
        BufferedInputStream buffIn = null;
        Object res = null;
        try{
            fileIn = context.openFileInput(fileName);
            buffIn = new BufferedInputStream(fileIn);
            objIn = new ObjectInputStream(buffIn);
            res = objIn.readObject();
        }
        catch(FileNotFoundException e){
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        finally {
            if (objIn != null){
                try{
                    objIn.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }

            if (buffIn != null){
                try{
                    buffIn.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }

            if (fileIn != null){
                try{
                    fileIn.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }
        }

        return res;
    }
}
