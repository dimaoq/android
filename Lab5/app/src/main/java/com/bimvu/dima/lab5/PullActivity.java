package com.bimvu.dima.lab5;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Xml;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import adapter.WeatherAdapter;
import data.Weather;

public class PullActivity extends AppCompatActivity {
    private static final String URL_STRING = "http://api.openweathermap.org/data/2.5/forecast?q=Yoshkar-Ola&mode=xml&appid=b1b15e88fa797225412429c1c50c122a";
    private ListView lstView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull);

        lstView = (ListView) findViewById(R.id.lstPullWeather);

        (new ParsePullXML()).execute();
    }

    class ParsePullXML extends AsyncTask<Context, Void, Void>{
        private ArrayList<Weather> weatherList;

        @Override
        protected Void doInBackground(Context... params) {
            InputStream is = null;
            InputStreamReader isReader = null;
            try{
                URL url = new URL(URL_STRING);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                is = connection.getInputStream();

                if (null != is){
                    weatherList = new ArrayList<Weather>();
                    isReader = new InputStreamReader(is);
                    XmlPullParser parser = Xml.newPullParser();
                    parser.setInput(isReader);

                    String tagName = "";
                    String weatherTimeFrom = "", weatherTimeTo = "", weatherTemperature = "",
                            weatherTemperatureUnit = "", weatherHumidity = "", weatherHumUnit = "";
                    int eventType = parser.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT){
                        tagName = parser.getName();

                        if (eventType == XmlPullParser.START_TAG){
                            if (tagName.equalsIgnoreCase("time")){
                                weatherTimeFrom = parser.getAttributeValue(null, "from");
                                weatherTimeTo = parser.getAttributeValue(null, "to");
                            } else if (tagName.equalsIgnoreCase("temperature")){
                                weatherTemperature = parser.getAttributeValue(null, "value");
                                weatherTemperatureUnit = parser.getAttributeValue(null, "unit");
                            } else if (tagName.equalsIgnoreCase("humidity")){
                                weatherHumidity = parser.getAttributeValue(null, "value");
                                weatherHumUnit = parser.getAttributeValue(null, "unit");
                            }
                        } else if (eventType == XmlPullParser.END_TAG){
                            if (tagName.equalsIgnoreCase("time")){
                                weatherList.add(new Weather(weatherTimeFrom, weatherTimeTo, Double.parseDouble(weatherTemperature),
                                        weatherTemperatureUnit, Integer.parseInt(weatherHumidity), weatherHumUnit));
                            }
                        }

                        eventType = parser.next();
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } finally {
                try{
                    if (null != is){
                        is.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try{
                    if (null != isReader){
                        isReader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (null != lstView && null != weatherList){
                WeatherAdapter adapter = new WeatherAdapter(PullActivity.this, weatherList);
                lstView.setAdapter(adapter);
            }
        }
    }
}
