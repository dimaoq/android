package com.bimvu.dima.lab6;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import data.WolframResult;

/*
1. Ключ приложения с wolfram alpha
https://www.wolframalpha.com/

2. Функциональные требования
- проверка подключенного интернета
- вывод модального прогресса при загрузке
- корректная обработка ошибок (отсутствие интернета/ошибки выисления/парсинга)
- доступные орерации - + - * /, скобки не надо
- возможность вычисления операций вида 4 + 6 * 10 / 2 и тд.

3. Требования в оформлению
- группировка классов по пэкэджам
- корректное использование модификаторов классов
- перенос строк/констант в соответсвующие файлы
 */

public class MainActivity extends AppCompatActivity {
    private static final String URL_STRING_PREFIX = "http://api.wolframalpha.com/v2/query?appid=KGG8WW-L672KLT3W9&input=";
    private TextView lblCalc = null;
    private TextView lblResult = null;
    private AlertDialog.Builder dialog = null;
    private ProgressDialog pdDialog = null;

    private void setButtonListener(Button btn, final String newText) {
        if (btn != null) {
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String curText = lblCalc.getText().toString();
                    lblCalc.setText(String.format("%s%s", curText, newText));
                }
            });
        }
    }

    private Boolean checkForInternetConnection(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblCalc = (TextView)findViewById(R.id.lblCalc);
        lblResult = (TextView)findViewById(R.id.lblResult);

        setButtonListener((Button) findViewById(R.id.btn0), "0");
        setButtonListener((Button) findViewById(R.id.btn1), "1");
        setButtonListener((Button) findViewById(R.id.btn2), "2");
        setButtonListener((Button) findViewById(R.id.btn3), "3");
        setButtonListener((Button) findViewById(R.id.btn4), "4");
        setButtonListener((Button) findViewById(R.id.btn5), "5");
        setButtonListener((Button) findViewById(R.id.btn6), "6");
        setButtonListener((Button) findViewById(R.id.btn7), "7");
        setButtonListener((Button) findViewById(R.id.btn8), "8");
        setButtonListener((Button) findViewById(R.id.btn9), "9");
        setButtonListener((Button) findViewById(R.id.btnPlus), "+");
        setButtonListener((Button) findViewById(R.id.btnMinus), "-");
        setButtonListener((Button) findViewById(R.id.btnMult), "*");
        setButtonListener((Button) findViewById(R.id.btnDiv), "/");
        setButtonListener((Button) findViewById(R.id.btnDot), ".");

        Button btnClear = (Button) findViewById(R.id.btnClear);
        if (btnClear != null) {
            btnClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lblCalc.setText("");
                }
            });
        }

        Button btnEnter = (Button) findViewById(R.id.btnEnter);
        if (btnEnter != null) {
            btnEnter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkForInternetConnection()){
                        (new ParseResult()).execute();
                    }
                    else{
                        if (dialog == null){
                            dialog = new AlertDialog.Builder(MainActivity.this);
                            dialog.setTitle(R.string.string_errorTitle);
                            dialog.setMessage(R.string.string_noInternetMsg);
                            dialog.setNegativeButton(R.string.string_OK, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
                        }

                        dialog.show();
                    }
                }
            });
        }
    }

    class ParseResult extends AsyncTask<Context, Void, Void>{
        private String result = "";
        private String calcString = "";
        private Boolean isError = false;
        private Boolean isParsingError = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            calcString = lblCalc.getText().toString();
            if (pdDialog == null)
            {
                pdDialog = new ProgressDialog(MainActivity.this);
                pdDialog.setMessage(getString(R.string.string_Calculating));
            }
            pdDialog.show();
        }

        @Override
        protected Void doInBackground(Context... params) {
            InputStream is = null;
            try {
                String suffix = URLEncoder.encode(calcString, "UTF-8");
                URL url = new URL(String.format("%s%s", URL_STRING_PREFIX, suffix));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                is = connection.getInputStream();

                if (null != is) {
                    try{
                        Serializer serializer = new Persister();
                        WolframResult wolframResult = serializer.read(WolframResult.class, is);
                        if (!wolframResult.isSuccess() || wolframResult.isError()){
                            isError = true;
                        }
                        else{
                            Boolean needExit = false;
                            for (WolframResult.Pod pod : wolframResult.getPods()){
                                if (pod.getTitle().equalsIgnoreCase("Result") ||
                                    pod.getTitle().equalsIgnoreCase("Exact result") ||
                                    pod.getTitle().equalsIgnoreCase("Decimal form") ||
                                    pod.getTitle().equalsIgnoreCase("Decimal approximation")){
                                    for (WolframResult.Pod.SubPod subPod : pod.getSubPods()){
                                        if (subPod.getTitle().length() == 0){
                                            result = subPod.getResult();
                                            needExit = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!needExit)
                            {
                                isParsingError = true;
                            }
                        }
                    } catch (Exception e) {
                        isParsingError = true;
                        e.printStackTrace();
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally{
                try{
                    if (null != is){
                        is.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (pdDialog != null)
            {
                pdDialog.cancel();
            }

            if (lblResult != null) {
                if (isError) {
                    lblResult.setText(R.string.string_calcErrorMsg);
                    return;
                }

                if (isParsingError) {
                    lblResult.setText(R.string.string_parseErrorMsg);
                    return;
                }

                lblResult.setText(String.format("= %s", result));
            }
        }
    }
}
