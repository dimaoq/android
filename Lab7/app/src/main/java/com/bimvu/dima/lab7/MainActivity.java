package com.bimvu.dima.lab7;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import com.bimvu.dima.lab7.adapter.CityAdapter;
import com.bimvu.dima.lab7.data.City;
import com.bimvu.dima.lab7.fragment.CityFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/*
1. 1 активити
2. при старте В РАЗМЕТКЕ добавить фрагмент со списком (ListView)
3. при старте приложения асинхронно (AsyncTask) добавить модель данных (заголовок - описание), загрузить в контроллер списка заголовки данных модели.
4. после загрузки модели в контроллер ДОБАВИТЬ НА ЭКРАН ФРАГМЕНТ ИЗ КОДА с описанием во второй фрагмент
5. по нажатию на элемент списка обновляем описание во втором фрагменте. НЕ ЗАМЕНЯЕМ ФРАГМЕНТ ПОЛНОСТЬЮ КАК В ПРИМЕРЕ, а обновляем данные ЧЕРЕЗ РЕАЛИЗАЦИЮ ИНТЕРФЕЙСА.
6. взаимодействие фрагмент - активити - фрагмент через интерфейсы.
 */
public class MainActivity extends AppCompatActivity implements CityFragment.OnCityListened, OnMapReadyCallback {
    private static final String FRAGMENT_MAP_TAG = "fragmentMap";
    private CityFragment cityFragment = null;
    private SupportMapFragment mapFragment = null;
    private GoogleMap mMap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cityFragment = (CityFragment)(getSupportFragmentManager().findFragmentById(R.id.fragmentCities));
        (new FillDataAsync()).execute();
    }

    private void createMapFragment(){
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.layoutMap);
        if (null != mainLayout){
            getSupportFragmentManager().beginTransaction().add(mainLayout.getId(), SupportMapFragment.newInstance(), FRAGMENT_MAP_TAG).commit();
            getSupportFragmentManager().executePendingTransactions();
            mapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentByTag(FRAGMENT_MAP_TAG);
            if (null != mapFragment){
                mapFragment.getMapAsync(this);
            }
        }
    }

    @Override
    public void setCity(City city) {
        if (null != mMap){
            LatLng newCity = new LatLng(city.getLatitude(), city.getLongitude());
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(newCity).title(String.format("Marker at %s", city.getCityName())));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newCity, 6));
        }
    }

    class FillDataAsync extends AsyncTask<Context, Void, Void>{
        private CityAdapter adapter = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            adapter = new CityAdapter(MainActivity.this);
            if (null != cityFragment){
                cityFragment.setAdapter(adapter);
            }
        }

        @Override
        protected Void doInBackground(Context... params) {
            ArrayList<City> allCities = new ArrayList<>();
            allCities.add(new City("Moscow", 55.751244, 37.618423));
            allCities.add(new City("London", 51.500152, -0.126236));
            allCities.add(new City("Paris", 48.864716, 2.349014));
            allCities.add(new City("Madrid", 40.416775, -3.703790));
            allCities.add(new City("Berlin", 52.520008, 13.404954));
            allCities.add(new City("Beijing", 39.92889, 116.38833));
            allCities.add(new City("New Delhi", 28.635308, 77.22496));
            allCities.add(new City("New York", 40.7141667, -74.0063889));
            allCities.add(new City("Sydney", -33.8830555556, 151.216666667));
            allCities.add(new City("Atlanta", 33.7488889, -84.3880556));

            for (City city : allCities){
                adapter.addCity(city);
                adapter.notifyDataSetChanged();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            createMapFragment();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
}
