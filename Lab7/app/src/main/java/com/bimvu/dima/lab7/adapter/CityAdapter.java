package com.bimvu.dima.lab7.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bimvu.dima.lab7.R;
import com.bimvu.dima.lab7.data.City;

import java.util.ArrayList;

public class CityAdapter extends BaseAdapter{
    private LayoutInflater layoutInflater;
    private ArrayList<City> cities = new ArrayList<>();

    public CityAdapter(Context context){
        layoutInflater = LayoutInflater.from(context);
    }

    public void addCity(City city){
        cities.add(city);
    }

    @Override
    public int getCount() {
        return cities.size();
    }

    @Override
    public Object getItem(int position) {
        return cities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (null == view){
            view = layoutInflater.inflate(R.layout.item_city, parent, false);
        }

        City city = cities.get(position);
        ((TextView)view.findViewById(R.id.lblCityName)).setText(city.getCityName());
        ((TextView)view.findViewById(R.id.lblLat)).setText(String.valueOf(city.getLatitude()));
        ((TextView)view.findViewById(R.id.lblLong)).setText(String.valueOf(city.getLongitude()));

        return view;
    }
}
