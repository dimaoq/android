package com.bimvu.dima.lab7.data;

public class City {

    private String mName;
    private double mLat;
    private double mLong;

    public City(String name, double lat, double longitude){
        mName = name;
        mLat = lat;
        mLong = longitude;
    }

    public String getCityName() {
        return mName;
    }

    public double getLatitude() {
        return mLat;
    }

    public double getLongitude() {
        return mLong;
    }
}
