package com.bimvu.dima.lab7.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bimvu.dima.lab7.R;
import com.bimvu.dima.lab7.adapter.CityAdapter;
import com.bimvu.dima.lab7.data.City;

public class CityFragment extends Fragment {
    private ListView lstCities = null;
    private OnCityListened parentActivity = null;

    public CityFragment() {
    }

    public void setAdapter(CityAdapter adapter){
        if (null != lstCities){
            lstCities.setAdapter(adapter);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city, container, false);
        lstCities = (ListView)view.findViewById(R.id.lstCities);
        if (null != lstCities){
            lstCities.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (null != parentActivity){
                        City city = (City) lstCities.getItemAtPosition(position);
                        parentActivity.setCity(city);
                    }
                }
            });
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCityListened) {
            parentActivity = (OnCityListened) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCityListened");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        parentActivity = null;
    }

    public interface OnCityListened {
        void setCity(City city);
    }
}
