package com.bimvu.dima.lab8;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class InformationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        Intent intent = getIntent();
        if (null != intent){
            TextView lblTitle = (TextView) findViewById(R.id.lblMarkerTitle);
            TextView lblSnippet = (TextView) findViewById(R.id.lblMarkerSnippet);

            Bundle bundle = intent.getExtras();
            if (null != bundle){
                String markerTitle = bundle.getString(MainActivity.INTENT_TITLE_KEY, "");
                String markerSnippet = bundle.getString(MainActivity.INTENT_SNIPPET_KEY, "");

                lblTitle.setText(markerTitle);
                lblSnippet.setText(markerSnippet);
            }
        }
    }
}
