package com.bimvu.dima.lab8;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Locale;

/*
- на экране 2 поля ввода для широты и долготы и кнопка установки маркера
- на экране карта (на том же экране что и поля ввода с кнопкой), по нажатию добавляем маркер на карту
- при добавлении маркера на карты КАРТА ВПИСЫВАЕТ (зумится, двигается) все маркеры на экране (все добавленные маркеры должны быть видимыми)
- при старте приложения на карты должны быть добавлено несколько маркеров
- иконка маркера кастомная
- балон маркера кастомный ( с сообщением)
- по нажатию на балон переход на другую активность с описанием (пропертей объекта)
 */

public class MainActivity extends FragmentActivity implements OnMapReadyCallback {
    public static final String INTENT_TITLE_KEY = "title";
    public static final String INTENT_SNIPPET_KEY = "snippet";

    private static final String TAG_ERROR = "ERR";
    private final String DEFAULT_MARKER_TITLE = "Default marker";
    private final String NEW_MARKER_TITLE = "New marker";
    private final int MAP_ZOOM_PADDING = 20;

    private GoogleMap mMap = null;
    private Button btnSetMarker = null;
    private EditText txtLatitude = null;
    private EditText txtLongitude = null;
    private LatLngBounds.Builder markerBoundsBuilder = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtLatitude = (EditText) findViewById(R.id.txtLat);
        txtLongitude = (EditText) findViewById(R.id.txtLong);
        btnSetMarker = (Button) findViewById(R.id.btnSetMarker);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragmentMap);
        mapFragment.getMapAsync(this);

        btnSetMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null == mMap){
                    return;
                }

                try {
                    double latitude = Double.parseDouble(txtLatitude.getText().toString());
                    double longitude = Double.parseDouble(txtLongitude.getText().toString());

                    LatLng newPlace = new LatLng(latitude, longitude);
                    markerBoundsBuilder.include(newPlace);

                    MarkerOptions options = new MarkerOptions();
                    options.position(newPlace);
                    options.title(NEW_MARKER_TITLE);
                    options.snippet(String.format("Latitude: %s\nLongitude: %s\nNo additional information provided",
                                    txtLatitude.getText().toString(), txtLongitude.getText().toString()));
                    options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon));
                    mMap.addMarker(options);

                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(markerBoundsBuilder.build(), MAP_ZOOM_PADDING);
                    mMap.animateCamera(cameraUpdate);
                }
                catch (Exception e){
                    Log.d(TAG_ERROR, e.getMessage());
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        markerBoundsBuilder = new LatLngBounds.Builder();

        final ArrayList<LatLng> startMarkers = new ArrayList<>();
        startMarkers.add(new LatLng(55.751244, 37.618423));
        startMarkers.add(new LatLng(48.864716, 2.349014));
        startMarkers.add(new LatLng(52.520008, 13.404954));
        startMarkers.add(new LatLng(28.635308, 77.22496));
        startMarkers.add(new LatLng(40.416775, -3.703790));

        final ArrayList<String> defaultSnippets = new ArrayList<>();
        defaultSnippets.add(String.format(Locale.US, "Moscow\nLatitude: %.3f\nLongitude: %.3f", startMarkers.get(0).latitude, startMarkers.get(0).longitude));
        defaultSnippets.add(String.format(Locale.US, "Paris\nLatitude: %.3f\nLongitude: %.3f", startMarkers.get(1).latitude, startMarkers.get(0).longitude));
        defaultSnippets.add(String.format(Locale.US, "Berlin\nLatitude: %.3f\nLongitude: %.3f", startMarkers.get(2).latitude, startMarkers.get(0).longitude));
        defaultSnippets.add(String.format(Locale.US, "New Delhi\nLatitude: %.3f\nLongitude: %.3f", startMarkers.get(3).latitude, startMarkers.get(0).longitude));
        defaultSnippets.add(String.format(Locale.US, "Madrid\nLatitude: %.3f\nLongitude: %.3f", startMarkers.get(4).latitude, startMarkers.get(0).longitude));

        int i = 0;
        for (LatLng position : startMarkers){
            markerBoundsBuilder.include(position);
            MarkerOptions options = new MarkerOptions();
            options.position(position);
            options.title(DEFAULT_MARKER_TITLE);
            options.snippet(defaultSnippets.get(i++));
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon));
            mMap.addMarker(options);
        }

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent intent = new Intent(MainActivity.this, InformationActivity.class);
                intent.putExtra(INTENT_TITLE_KEY, marker.getTitle());
                intent.putExtra(INTENT_SNIPPET_KEY, marker.getSnippet());
                startActivity(intent);
            }
        });
        //CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(markerBoundsBuilder.build(), 1);
        //mMap.animateCamera(cameraUpdate);
    }
}
