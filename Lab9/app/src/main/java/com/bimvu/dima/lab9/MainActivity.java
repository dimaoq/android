package com.bimvu.dima.lab9;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import service.WolframService;
import service.WolframService.ResultVariants;
import service.WolframService.WolframServiceResult;
import service.WolframService.WolframBinder;

public class MainActivity extends AppCompatActivity {
    private final static String DEBUG_TAG = "DEBUG_TAG";

    private TextView lblCalc = null;
    private TextView lblResult = null;
    private AlertDialog.Builder mAlertDialog = null;
    private ProgressDialog mPdDialog = null;
    private WolframService mService = null;
    private boolean mIsBound = false;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            WolframBinder binder = (WolframBinder) service;
            mService = binder.getService();
            mIsBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mIsBound = false;
        }
    };

    private void setButtonListener(Button btn, final String newText) {
        if (btn != null) {
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String curText = lblCalc.getText().toString();
                    lblCalc.setText(String.format("%s%s", curText, newText));
                }
            });
        }
    }

    private Boolean checkForInternetConnection(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblCalc = (TextView)findViewById(R.id.lblCalc);
        lblResult = (TextView)findViewById(R.id.lblResult);

        setButtonListener((Button) findViewById(R.id.btn0), "0");
        setButtonListener((Button) findViewById(R.id.btn1), "1");
        setButtonListener((Button) findViewById(R.id.btn2), "2");
        setButtonListener((Button) findViewById(R.id.btn3), "3");
        setButtonListener((Button) findViewById(R.id.btn4), "4");
        setButtonListener((Button) findViewById(R.id.btn5), "5");
        setButtonListener((Button) findViewById(R.id.btn6), "6");
        setButtonListener((Button) findViewById(R.id.btn7), "7");
        setButtonListener((Button) findViewById(R.id.btn8), "8");
        setButtonListener((Button) findViewById(R.id.btn9), "9");
        setButtonListener((Button) findViewById(R.id.btnPlus), "+");
        setButtonListener((Button) findViewById(R.id.btnMinus), "-");
        setButtonListener((Button) findViewById(R.id.btnMult), "*");
        setButtonListener((Button) findViewById(R.id.btnDiv), "/");
        setButtonListener((Button) findViewById(R.id.btnDot), ".");

        Button btnClear = (Button) findViewById(R.id.btnClear);
        if (btnClear != null) {
            btnClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lblCalc.setText("");
                }
            });
        }

        Button btnEnter = (Button) findViewById(R.id.btnEnter);
        if (btnEnter != null) {
            btnEnter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkForInternetConnection()){
                        if (mIsBound){
                            if (mPdDialog == null)
                            {
                                mPdDialog = new ProgressDialog(MainActivity.this);
                                mPdDialog.setMessage(getString(R.string.string_Calculating));
                            }
                            mPdDialog.show();

                            mService.startCalculateResult(lblCalc.getText().toString());
                        }
                        else{
                            Log.d(DEBUG_TAG, "Service is not bound");
                        }
                    }
                    else{
                        if (mAlertDialog == null){
                            mAlertDialog = new AlertDialog.Builder(MainActivity.this);
                            mAlertDialog.setTitle(R.string.string_errorTitle);
                            mAlertDialog.setMessage(R.string.string_noInternetMsg);
                            mAlertDialog.setNegativeButton(R.string.string_OK, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
                        }

                        mAlertDialog.show();
                    }
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent serviceIntent = new Intent(this, WolframService.class);
        PendingIntent pi = createPendingResult(WolframService.CODE_PENDING_INTENT, new Intent(), 0);
        serviceIntent.putExtra(WolframService.KEY_PENDING_INTENT, pi);
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mIsBound){
            unbindService(mConnection);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == WolframService.CODE_RETURN_RESULT && requestCode == WolframService.CODE_PENDING_INTENT){
            if (null != mPdDialog && mPdDialog.isShowing()){
                mPdDialog.cancel();
            }

            WolframServiceResult result = data.getParcelableExtra(WolframService.KEY_PARCEL_WOLFRAM_RESULT);
            ResultVariants var = result.getResultInformation();
            switch (var){
                case PARSING_ERROR:
                    lblResult.setText(R.string.string_parseErrorMsg);
                    break;
                case CALCULATING_ERROR:
                    lblResult.setText(R.string.string_calcErrorMsg);
                    break;
                case CALCULATED:
                    lblResult.setText(String.format("= %s", result.getResult()));
                    break;
            }
        }
    }
}
