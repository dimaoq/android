package data;

import android.os.AsyncTask;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import service.WolframService;

public class WolframParser extends AsyncTask<Void, Void, Void> {
    private static final String URL_STRING_PREFIX = "http://api.wolframalpha.com/v2/query?appid=KGG8WW-L672KLT3W9&input=";
    private String mResult;
    private String mCalcString;
    private Boolean mIsError;
    private Boolean mIsParsingError;
    private WolframService mService;

    public WolframParser(WolframService boundedService, String calcString){
        mService = boundedService;
        mCalcString = calcString;

        mResult = "";
        mIsError = false;
        mIsParsingError = false;
    }

    @Override
    protected Void doInBackground(Void... params) {
        InputStream is = null;
        try {
            String suffix = URLEncoder.encode(mCalcString, "UTF-8");
            URL url = new URL(String.format("%s%s", URL_STRING_PREFIX, suffix));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            is = connection.getInputStream();

            if (null != is) {
                try{
                    Serializer serializer = new Persister();
                    WolframXMLResult wolframResult = serializer.read(WolframXMLResult.class, is);
                    if (!wolframResult.isSuccess() || wolframResult.isError()){
                        mIsError = true;
                    }
                    else{
                        Boolean needExit = false;
                        for (WolframXMLResult.Pod pod : wolframResult.getPods()){
                            if (pod.getTitle().equalsIgnoreCase("Result") ||
                                    pod.getTitle().equalsIgnoreCase("Exact result") ||
                                    pod.getTitle().equalsIgnoreCase("Decimal form") ||
                                    pod.getTitle().equalsIgnoreCase("Decimal approximation")){
                                for (WolframXMLResult.Pod.SubPod subPod : pod.getSubPods()){
                                    if (subPod.getTitle().length() == 0){
                                        mResult = subPod.getResult();
                                        needExit = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (!needExit)
                        {
                            mIsParsingError = true;
                        }
                    }
                } catch (Exception e) {
                    mIsParsingError = true;
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            try{
                if (null != is){
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        WolframService.WolframServiceResult result;
        if (mIsError){
            result = new WolframService.WolframServiceResult(WolframService.ResultVariants.CALCULATING_ERROR);
        }
        else{
            if (mIsParsingError){
                result = new WolframService.WolframServiceResult(WolframService.ResultVariants.PARSING_ERROR);
            }
            else{
                result = new WolframService.WolframServiceResult(WolframService.ResultVariants.CALCULATED);
                result.setResult(mResult);
            }
        }
        mService.notifyGotResult(result);
    }
}