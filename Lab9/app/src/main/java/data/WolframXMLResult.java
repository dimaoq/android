package data;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root(name = "queryresult", strict = false)
public class WolframXMLResult {

    @Attribute(required = false)
    private String success;

    @Attribute(required = false)
    private String error;

    public Boolean isError() {
        return error.equalsIgnoreCase("true");
    }

    public Boolean isSuccess() {
        return success.equalsIgnoreCase("true");
    }

    @ElementList(name = "pod", required = false, inline = true)
    private ArrayList<Pod> pods;

    public ArrayList<Pod> getPods() {
        return pods;
    }

    @Root(name = "pod", strict = false)
    public static class Pod{
        @Attribute
        private String title;

        public String getTitle() {
            return title;
        }

        @ElementList(name = "subpod", required = false, inline = true)
        private ArrayList<SubPod> subPods;

        public ArrayList<SubPod> getSubPods() {
            return subPods;
        }

        @Root(name = "subpod", strict = false)
        public static class SubPod{
            @Attribute
            private String title;

            public String getTitle() {
                return title;
            }

            @Element(name = "plaintext", required = false)
            private String result;

            public String getResult() {
                return result;
            }
        }
    }
}
