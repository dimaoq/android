package service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

import com.bimvu.dima.lab9.MainActivity;

import data.WolframParser;

public class WolframService extends Service {
    public static final String KEY_PENDING_INTENT = "WolframService.PendingIntent";
    public static final String KEY_PARCEL_WOLFRAM_RESULT = "WolframService.ParcelableWolframResult";
    public static final int CODE_PENDING_INTENT = 322;
    public static final int CODE_RETURN_RESULT = 1227;
    private final IBinder mBinder = new WolframBinder();
    private PendingIntent mPendingIntent = null;

    public enum ResultVariants{
        CALCULATED,
        PARSING_ERROR,
        CALCULATING_ERROR
    }

    public static class WolframServiceResult implements Parcelable{
        private ResultVariants mResultVariant;
        private String mResult;

        public WolframServiceResult(ResultVariants variants){
            mResultVariant = variants;
            mResult = "";
        }

        protected WolframServiceResult(Parcel in) {
            mResultVariant = ResultVariants.valueOf(in.readString());
            mResult = in.readString();
        }

        public static final Creator<WolframServiceResult> CREATOR = new Creator<WolframServiceResult>() {
            @Override
            public WolframServiceResult createFromParcel(Parcel in) {
                return new WolframServiceResult(in);
            }

            @Override
            public WolframServiceResult[] newArray(int size) {
                return new WolframServiceResult[size];
            }
        };

        public ResultVariants getResultInformation(){
            return mResultVariant;
        }

        public void setResult(String newResult){
            mResult = newResult;
        }

        public String getResult(){
            return mResult;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mResultVariant.name());
            dest.writeString(mResult);
        }
    }

    public class WolframBinder extends Binder {
        public WolframService getService(){
            return WolframService.this;
        }
    }

    public WolframService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        mPendingIntent = intent.getParcelableExtra(KEY_PENDING_INTENT);
        return mBinder;
    }

    public void notifyGotResult(WolframServiceResult result){
        if (null != mPendingIntent){
            Intent sendIntent = new Intent(getApplicationContext(), MainActivity.class);
            sendIntent.putExtra(KEY_PARCEL_WOLFRAM_RESULT, result);
            try {
                mPendingIntent.send(getApplicationContext(), CODE_RETURN_RESULT, sendIntent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
        }
    }

    public void startCalculateResult(String strToCalc){
        WolframParser parser = new WolframParser(this, strToCalc);
        parser.execute();
    }
}
