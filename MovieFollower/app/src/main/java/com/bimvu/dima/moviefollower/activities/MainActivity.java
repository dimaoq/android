package com.bimvu.dima.moviefollower.activities;

import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.bimvu.dima.moviefollower.R;
import com.bimvu.dima.moviefollower.fragments.NowPlayingMovies;
import com.bimvu.dima.moviefollower.fragments.SearchMovie;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private NavigationView mNavView;
    private ActionBarDrawerToggle mActionBarDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //find all views
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mNavView = (NavigationView) findViewById(R.id.navView);

        setSupportActionBar(mToolbar);
        mActionBarDrawerToggle = setupDrawerToggle();
        setupDrawerContent();

        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        selectDrawerItem((mNavView.getMenu().getItem(0)));
    }

    private Boolean checkForInternetConnection(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }

    private ActionBarDrawerToggle setupDrawerToggle(){
        return new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close);
    }

    private void setupDrawerContent(){
        mNavView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                selectDrawerItem(item);
                return true;
            }
        });
    }

    private void selectDrawerItem(MenuItem item){

        item.setChecked(true);
        setTitle(item.getTitle());

        if (!checkForInternetConnection()){
            Toast noConnectionMsg = Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернетом", Toast.LENGTH_SHORT);
            noConnectionMsg.show();
            return;
        }

        Fragment fragment = null;
        Class fragmentClass = null;
        switch (item.getItemId()){
            case R.id.menu_inTheater:
                fragmentClass = NowPlayingMovies.class;
                break;
            case R.id.menu_Search:
                fragmentClass = SearchMovie.class;
                break;
        }
        if (null != fragmentClass){
            try{
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.contentLayout, fragment).commit();
        }

        mDrawerLayout.closeDrawers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mActionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        mActionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }
}
