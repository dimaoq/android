package com.bimvu.dima.moviefollower.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimvu.dima.moviefollower.R;
import com.bimvu.dima.moviefollower.interfaces.JSONReciever;
import com.bimvu.dima.moviefollower.utils.APIRequestCreator;
import com.bimvu.dima.moviefollower.utils.ImageLoader;
import com.bimvu.dima.moviefollower.utils.JSONLoader;
import com.bimvu.dima.moviefollower.utils.LoaderShower;

import org.json.JSONArray;
import org.json.JSONObject;

public class MovieActivity extends AppCompatActivity implements JSONReciever{
    public static final String INTENT_KEY = "__ID__";
    private int mID;

    private ImageView mImg;
    private TextView mOverview;
    private TextView mReleaseDate;
    private TextView mVote;
    private TextView mGenres;

    private LoaderShower mProgressShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        setTitle("");

        //get all views
        mImg = (ImageView) findViewById(R.id.imgBigPoster);
        mVote = (TextView) findViewById(R.id.lblVote);
        mOverview = (TextView) findViewById(R.id.lblMovieOverview);
        mReleaseDate = (TextView) findViewById(R.id.lblReleaseDate);
        mGenres = (TextView) findViewById(R.id.lblGenres);

        Intent intent = getIntent();
        if (null != intent){
            mID = intent.getIntExtra(INTENT_KEY, 0);
        }

        JSONLoader loader = new JSONLoader(APIRequestCreator.getMovie(mID), this);

        mProgressShow = new LoaderShower(this, loader);
        loader.execute();
        mProgressShow.show();
    }

    @Override
    public void notifyGotJSONResult(JSONObject movie) {
        try{
            String posterPath = movie.getString("poster_path");
            ImageLoader imgLoader = new ImageLoader(mImg);
            imgLoader.loadBigBitmap(posterPath);

            String title = movie.getString("title");
            setTitle(title);

            String overview = movie.getString("overview");
            mOverview.setText(overview);

            String vote = String.valueOf(movie.getDouble("vote_average"));
            mVote.setText(vote);

            String releaseDate = movie.getString("release_date");
            mReleaseDate.setText(releaseDate);

            String genresString = "";
            JSONArray genres = movie.getJSONArray("genres");
            for (int i = 0; i < genres.length(); ++i){
                JSONObject genre = genres.getJSONObject(i);
                genresString += genre.getString("name");
                if (i < genres.length() - 1){
                    genresString += ", ";
                }
            }
            mGenres.setText(genresString);
        } catch(Exception e){
            e.printStackTrace();
        }

        if (null != mProgressShow){
            mProgressShow.hide();
        }
    }
}
