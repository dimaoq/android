package com.bimvu.dima.moviefollower.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimvu.dima.moviefollower.R;
import com.bimvu.dima.moviefollower.activities.MovieActivity;
import com.bimvu.dima.moviefollower.data.Movie;
import com.bimvu.dima.moviefollower.utils.ImageLoader;

import java.util.ArrayList;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> {
    private ArrayList<Movie> mMovies;

    public static class MovieHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView mPoster;
        public TextView mTitle;
        public TextView mOverview;
        public int mID;
        private IMovieClick clickCallback;

        public MovieHolder(View v, IMovieClick callback){
            super(v);

            clickCallback = callback;
            mPoster = (ImageView) v.findViewById(R.id.imgPoster);
            mTitle = (TextView) v.findViewById(R.id.lblTitle);
            mOverview = (TextView) v.findViewById(R.id.lblOverview);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickCallback.onMovieClicked(mID);
        }

        public interface IMovieClick{
            void onMovieClicked(int id);
        }
    }

    public MovieAdapter(){
        mMovies = new ArrayList<>();
    }

    public void addMovie(Movie movie){
        mMovies.add(movie);
    }

    @Override
    public MovieAdapter.MovieHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);

        return new MovieHolder(v, new MovieHolder.IMovieClick() {
            @Override
            public void onMovieClicked(int id) {
                Intent intent = new Intent(parent.getContext(), MovieActivity.class);
                intent.putExtra(MovieActivity.INTENT_KEY, id);
                parent.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public void onBindViewHolder(MovieAdapter.MovieHolder holder, int position) {
        holder.mTitle.setText(mMovies.get(position).getTitle());
        holder.mOverview.setText(mMovies.get(position).getOverview());
        holder.mID = mMovies.get(position).getID();

        ImageLoader imgLoader = new ImageLoader(holder.mPoster);
        imgLoader.loadSmallBitmap(mMovies.get(position).getPosterPath());
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }


}
