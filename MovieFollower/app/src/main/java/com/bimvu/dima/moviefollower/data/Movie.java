package com.bimvu.dima.moviefollower.data;

public class Movie {
    private String mPosterPath;
    private String mTitle;
    private String mOverview;
    private int mID;

    public Movie(String title, String overview, String posterPath, int id) {
        mTitle = title;
        mOverview = overview;
        mPosterPath = posterPath;
        mID = id;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getOverview() {
        return mOverview;
    }

    public int getID() {
        return mID;
    }
}
