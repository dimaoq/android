package com.bimvu.dima.moviefollower.data;

public class MovieLittle extends Movie{
    public MovieLittle(String title, String overview, String posterPath, int id) {
        super(title, overview, posterPath, id);
    }
}
