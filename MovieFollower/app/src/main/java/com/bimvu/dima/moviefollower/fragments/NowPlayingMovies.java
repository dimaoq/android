package com.bimvu.dima.moviefollower.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bimvu.dima.moviefollower.R;
import com.bimvu.dima.moviefollower.adapters.MovieAdapter;
import com.bimvu.dima.moviefollower.data.MovieLittle;
import com.bimvu.dima.moviefollower.interfaces.JSONReciever;
import com.bimvu.dima.moviefollower.utils.APIRequestCreator;
import com.bimvu.dima.moviefollower.utils.JSONLoader;
import com.bimvu.dima.moviefollower.utils.LoaderShower;

import org.json.JSONArray;
import org.json.JSONObject;

public class NowPlayingMovies extends Fragment implements JSONReciever{
    private RecyclerView lstMovies;
    private RecyclerView.Adapter mAdapter;
    private LoaderShower mProgressShow;

    public NowPlayingMovies() {
    }

    public static NowPlayingMovies newInstance() {
        NowPlayingMovies fragment = new NowPlayingMovies();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONLoader loader = new JSONLoader(APIRequestCreator.getNowPlayingMovies(), this);

        mProgressShow = new LoaderShower(getContext(), loader);
        loader.execute();
        mProgressShow.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_now_playing_movies, container, false);

        lstMovies = (RecyclerView) view.findViewById(R.id.lstFilms);
        lstMovies.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        lstMovies.setLayoutManager(mLayoutManager);

        return view;
    }

    @Override
    public void notifyGotJSONResult(JSONObject jsonObject) {
        mAdapter = new MovieAdapter();
        lstMovies.setAdapter(mAdapter);
        try{
            JSONArray results = jsonObject.getJSONArray("results");
            for (int i = 0; i < results.length(); ++i){
                JSONObject movie = results.getJSONObject(i);

                String posterPath = movie.getString("poster_path");
                String title = movie.getString("title");
                String overview = movie.getString("overview");
                int id = movie.getInt("id");

                MovieLittle newMovie = new MovieLittle(title, overview, posterPath, id);
                ((MovieAdapter) mAdapter).addMovie(newMovie);

                mAdapter.notifyDataSetChanged();
            }
        } catch(Exception e){
            e.printStackTrace();
        }

        if (null != mProgressShow){
            mProgressShow.hide();
        }
    }
}
