package com.bimvu.dima.moviefollower.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.bimvu.dima.moviefollower.R;
import com.bimvu.dima.moviefollower.adapters.MovieAdapter;
import com.bimvu.dima.moviefollower.data.MovieLittle;
import com.bimvu.dima.moviefollower.interfaces.JSONReciever;
import com.bimvu.dima.moviefollower.utils.APIRequestCreator;
import com.bimvu.dima.moviefollower.utils.JSONLoader;
import com.bimvu.dima.moviefollower.utils.LoaderShower;

import org.json.JSONArray;
import org.json.JSONObject;

public class SearchMovie extends Fragment implements JSONReciever{
    private RecyclerView lstMovies;
    private EditText txtMovieTitle;
    private MovieAdapter mMovieAdapter;
    private LoaderShower mProgressShow;

    public SearchMovie() {
    }

    public static SearchMovie newInstance() {
        SearchMovie fragment = new SearchMovie();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_movie, container, false);

        lstMovies = (RecyclerView) view.findViewById(R.id.lstSearchFilms);
        lstMovies.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        lstMovies.setLayoutManager(mLayoutManager);

        mMovieAdapter = new MovieAdapter();
        lstMovies.setAdapter(mMovieAdapter);

        txtMovieTitle = (EditText) view.findViewById(R.id.txtMovieName);
        txtMovieTitle.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN &&
                        keyCode == KeyEvent.KEYCODE_ENTER){

                    InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(txtMovieTitle.getWindowToken(), 0);

                    JSONLoader loader = new JSONLoader(APIRequestCreator.getSearchMovies(txtMovieTitle.getText().toString()), SearchMovie.this);
                    mProgressShow = new LoaderShower(getContext(), loader);
                    loader.execute();
                    mProgressShow.show();

                    return true;
                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void notifyGotJSONResult(JSONObject jsonObject) {
        mMovieAdapter = new MovieAdapter();
        lstMovies.setAdapter(mMovieAdapter);
        try{
            JSONArray results = jsonObject.getJSONArray("results");
            for (int i = 0; i < results.length(); ++i){
                JSONObject movie = results.getJSONObject(i);

                String posterPath = movie.getString("poster_path");
                String title = movie.getString("title");
                String overview = movie.getString("overview");
                int id = movie.getInt("id");

                MovieLittle newMovie = new MovieLittle(title, overview, posterPath, id);
                mMovieAdapter.addMovie(newMovie);

                mMovieAdapter.notifyDataSetChanged();
            }
        } catch(Exception e){
            e.printStackTrace();
        }

        if (null != mProgressShow){
            mProgressShow.hide();
        }
    }
}
