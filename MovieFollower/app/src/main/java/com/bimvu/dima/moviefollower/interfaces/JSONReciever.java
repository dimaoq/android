package com.bimvu.dima.moviefollower.interfaces;

import org.json.JSONObject;

public interface JSONReciever {
    void notifyGotJSONResult(JSONObject jsonObject);
}
