package com.bimvu.dima.moviefollower.utils;

import java.net.URLEncoder;

public class APIRequestCreator {
    static private final String API_REQUEST_PREFIX = "http://api.themoviedb.org/3/";
    static private final String API_LANGUAGE = "language=en&";
    static private final String API_KEY = "api_key=1e9ba655d7a7cdc01d9ec9fe49cafe71";

    static public String getNowPlayingMovies(){
        return API_REQUEST_PREFIX + "movie/now_playing?" + API_LANGUAGE + API_KEY;
    }

    static public String getSearchMovies(String movieTitle){
        String prefix = API_REQUEST_PREFIX + "search/movie?query=";
        try {
            String validMovieTitle = URLEncoder.encode(movieTitle, "utf-8");
            return prefix + validMovieTitle + "&" + API_LANGUAGE + API_KEY;
        } catch (Exception e){
            e.printStackTrace();
        }

        return "";
    }

    static public String getMovie(int id){
        return API_REQUEST_PREFIX + "movie/" + String.valueOf(id) + "?" + API_LANGUAGE + API_KEY;
    }
}
