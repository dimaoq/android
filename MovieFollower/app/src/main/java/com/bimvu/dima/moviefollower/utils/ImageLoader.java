package com.bimvu.dima.moviefollower.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class ImageLoader {
    static private String IMG_SERVER_PATH = "http://image.tmdb.org/t/p/";
    static private Map<String, Bitmap> smallBitmapMap = new HashMap<>();
    static private Map<String, Bitmap> bigBitmapMap = new HashMap<>();

    private ImageView mImageView;

    public ImageLoader(ImageView imgView){
        mImageView = imgView;
    }

    public void loadSmallBitmap(String imgPath){
        if (smallBitmapMap.containsKey(imgPath)){
            setBitmap(smallBitmapMap.get(imgPath), "", smallBitmapMap);
        }
        else{
            (new ImageDownloader(IMG_SERVER_PATH + "w92" + imgPath, imgPath, smallBitmapMap)).execute();
        }
    }

    public void loadBigBitmap(String imgPath){
        if (bigBitmapMap.containsKey(imgPath)){
            setBitmap(bigBitmapMap.get(imgPath), "", bigBitmapMap);
        }
        else{
            (new ImageDownloader(IMG_SERVER_PATH + "w500" + imgPath, imgPath, bigBitmapMap)).execute();
        }
    }

    private void setBitmap(Bitmap bmp, String addToMap, Map mapToAdd){
        if (addToMap.length() > 0){
            mapToAdd.put(addToMap, bmp);
        }
        mImageView.setImageBitmap(bmp);
    }

    private class ImageDownloader extends AsyncTask<Void, Void, Void>{
        private String mUrl;
        private String mImg;
        private Bitmap mBmp;
        private Map mMap;

        public ImageDownloader(String url, String imgPath, Map map){
            mUrl = url;
            mImg = imgPath;
            mBmp = null;
            mMap = map;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try{
                URL url = new URL(mUrl);
                mBmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (null != mBmp){
                setBitmap(mBmp, mImg, mMap);
            }
        }
    }
}
