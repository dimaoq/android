package com.bimvu.dima.moviefollower.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.bimvu.dima.moviefollower.interfaces.JSONReciever;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JSONLoader extends AsyncTask<Void, Void, Void>{
    private String mURL;
    private StringBuilder jsonString;
    private JSONReciever mReciever;

    public JSONLoader(String url, JSONReciever reciever){
        mURL = url;
        mReciever = reciever;
        jsonString = new StringBuilder();
    }

    @Override
    protected Void doInBackground(Void... params) {
        InputStream is = null;
        try{
            URL url = new URL(mURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            is = connection.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine()) != null){
                jsonString.append(line).append('\n');
            }

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if (null != is){
                try{
                    is.close();
                } catch (Exception e){

                }
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        try{
            JSONObject jsonObject = new JSONObject(jsonString.toString());
            mReciever.notifyGotJSONResult(jsonObject);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
