package com.bimvu.dima.moviefollower.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

public class LoaderShower {
    private ProgressDialog mProgressDialog;

    public LoaderShower(Context context, final JSONLoader cancelLoader){
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Загрузка...");
        mProgressDialog.setButton(Dialog.BUTTON_NEGATIVE, "Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelLoader.cancel(true);
            }
        });
    }

    public void show(){
        mProgressDialog.show();
    }

    public void hide(){
        mProgressDialog.cancel();
    }
}
